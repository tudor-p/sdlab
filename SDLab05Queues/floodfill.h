#pragma once
#include <cstdint>

typedef uint16_t FLOODFILL_COLOR_t;

FLOODFILL_COLOR_t **FLOODFILL_readMap(char const * path, int &rows, int &cols);
void FLOODFILL_deleteMap(FLOODFILL_COLOR_t **&m, int rows);
void FLOODFILL_floodfill(FLOODFILL_COLOR_t **map, int r, int c, FLOODFILL_COLOR_t new_color);
