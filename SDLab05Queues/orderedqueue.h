#pragma once
#include <cstdint>

typedef uint16_t ORDEREDQUEUE_INDEX_t;
typedef uint8_t ORDEREDQUEUE_DATA_t;
struct OrderedQueue_t {
	ORDEREDQUEUE_INDEX_t head, tail;
	ORDEREDQUEUE_INDEX_t capacity;
	ORDEREDQUEUE_DATA_t *queue;	
};

//init the queue (head, tail, capacity and allocates space for the queue data vector)
//returns true when the memory got allocated corectly
bool ORDEREDQUEUE_init(OrderedQueue_t &q, ORDEREDQUEUE_INDEX_t capacity);
//deallocates the reserved memory
void ORDEREDQUEUE_delete(OrderedQueue_t &q);
//returns true when the queue is empty
bool ORDEREDQUEUE_is_empty(OrderedQueue_t const &q);
//returns true when the queue is full
bool ORDEREDQUEUE_is_full(OrderedQueue_t const &q);
//puts an element at the tail of the queue
//returns true if the queue still had space for the element
bool ORDEREDQUEUE_put(OrderedQueue_t &q, ORDEREDQUEUE_DATA_t data);
//trys to get the element at the head of the queue out to data
//returns true when the queue was not empty
bool ORDEREDQUEUE_get(OrderedQueue_t &q, ORDEREDQUEUE_DATA_t &data);
