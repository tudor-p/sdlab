#pragma once
#pragma once
#include <cstdint>

typedef struct {
	uint16_t r, c;
} LINKEDQUEUE_DATA_t;
struct LinkedQueueNode_t {
	LinkedQueueNode_t *next;
	LINKEDQUEUE_DATA_t data;
};
struct LinkedQueue_t {
	LinkedQueueNode_t *head, *tail;
};

//init the queue (head, tail)
void LINKEDQUEUE_init(LinkedQueue_t &q);
//deallocates all the nodes from the memory
void LINKEDQUEUE_delete(LinkedQueue_t &q);
//returns true when the queue is empty
bool LINKEDQUEUE_is_empty(LinkedQueue_t const &q);
//puts an element at the tail of the queue
//returns true if the queue still had space for the element
bool LINKEDQUEUE_put(LinkedQueue_t &q, LINKEDQUEUE_DATA_t data);
//trys to get the element at the head of the queue out to data
//returns true when the queue was not empty
bool LINKEDQUEUE_get(LinkedQueue_t &q, LINKEDQUEUE_DATA_t &data);
