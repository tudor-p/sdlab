#include <fstream>
#include <iostream>
#include "floodfill.h"
#include "linkedqueue.h"

//try to read from the file given by the path the map matrix
// returns pointer to the newly allocated matrix and the number of rows and cols by reference
// returns null if the file could not be open
// does not check that the file is properly formatted or that the info is complete
FLOODFILL_COLOR_t **FLOODFILL_readMap(char const * path, int &rows, int &cols) {
	std::ifstream fin(path);
	if (!(fin.is_open())) return NULL;
	fin >> rows >> cols;
	FLOODFILL_COLOR_t ** m = new FLOODFILL_COLOR_t*[rows];
	for (int r = 0; r < rows; ++r) {
		m[r] = new FLOODFILL_COLOR_t[cols];
		for (int c = 0; c < cols; ++c) {
			fin >> m[r][c];
		}
	}
	return m;
}

void FLOODFILL_floodfill(FLOODFILL_COLOR_t **map, int r, int c, FLOODFILL_COLOR_t new_color) {
	LinkedQueue_t q;
	LINKEDQUEUE_init(q);
	//TODO: implement...
}

