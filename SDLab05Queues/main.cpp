#include <iostream>
#include "orderedqueue.h"
#include "floodfill.h"
using namespace std;
void P4() {
	OrderedQueue_t  q;
	ORDEREDQUEUE_init(q, 4);

	for (int i = 0; i < 5; ++i) {
		cout << "Pun " << i << " in coada. Rezultat: " << ORDEREDQUEUE_put(q, i) << endl;
	}

	for (int i = 0; i < 5; ++i) {
		ORDEREDQUEUE_DATA_t data;
		bool res = ORDEREDQUEUE_get(q, data);
		cout << "Iau din coada " << i << " " << ((int)data) << " " << res << endl;
	}
	ORDEREDQUEUE_delete(q);
}
void display(FLOODFILL_COLOR_t const * const * const m, int rows, int cols) {
	for (int r = 0; r < rows; ++r)
	{
		cout << endl;
		for (int c = 0; c < cols; ++c)
		{
			cout << " " << m[r][c];
		}
	}
	cout << endl;
}
void P6() {
	int rows, cols;
	FLOODFILL_COLOR_t **map = FLOODFILL_readMap("0smallmap.txt", rows, cols);
	if (NULL == map) {
		cout << "Maybe the file does not exist?" << endl;
		return;
	}
	display(map, rows, cols);
	int r, c, color;
	cout << "enter row column and new color: ";
	cin >> r >> c >> color;
	FLOODFILL_floodfill(map, r, c, color);
	display(map,rows, cols);
	FLOODFILL_deleteMap(map, rows);
}
int main() {
	P6();
	
	cin.clear();
	cin.ignore();
	cin.get();
	return 0;
}