#include <iostream>
#include <ctime>
using namespace std;

#define DEBUG_MODE

typedef void* Atom_t;

struct AVL_Node_t {
	Atom_t data;
	int bf;
	AVL_Node_t *left, *right;
};

typedef int(*AVL_cmp_func_t)(void const * data_ptr1, void const * data_ptr2);


///Balancing rotations
void AVL_rotate_right_simple(AVL_Node_t *&a)
{
	AVL_Node_t *b;
	b = a->left;
	a->left = b->right;
	b->right = a;
	a->bf = 0;
	b->bf = 0;
	a = b;
}


void AVL_rotate_left_simple(AVL_Node_t *&a)
{
	throw exception("not implemented yet! Work to do here!!!");
}


void AVL_roate_left_double(AVL_Node_t *&a) {
	throw exception("not implemented yet! Work to do here!!!");
}


void AVL_rotate_right_double(AVL_Node_t *&a) {
	throw exception("not implemented yet! Work to do here!!!");
}


///updates the references to:
///  1) the feature parent of the node to be added
///  2) the critical node detected
///  3) the parent of the detected critical node
///uses:
///   a) the pointer to the root of the tree
///   b) the value to be inserted (for deciding the exploration direction)
///   c) the comparison function (optional! needed only for generic trees)
///
///returns:
/// true - when the value to attach does not exist
/// false - the value to attach already exists in the tree
bool AVL_get_the_future_parent_of_node_to_add_and_critical_node_and_its_parent(
	AVL_Node_t *root_ptr
	, Atom_t const value
	, AVL_Node_t *&parent_for_new_node
	, AVL_Node_t *&critical_node
	, AVL_Node_t*&critical_node_parent
	, AVL_cmp_func_t cmp) {
	throw exception("not implemented yet! Work to do here!!!");
}


void AVL_attach_new_node(
	AVL_Node_t *&root_ptr
	, AVL_Node_t *&critical_node
	, Atom_t x
	, AVL_Node_t *parent_for_new_node
	, AVL_cmp_func_t cmp) {
	throw exception("not implemented yet! Work to do here!!!");
}


void AVL_update_balance_factor_down_of_including(
	AVL_Node_t *node
	, Atom_t x
	, AVL_cmp_func_t cmp) {
	throw exception("not implemented yet! Work to do here!!!");
}


void AVL_detect_and_fix_unbalance(AVL_Node_t *&root_ptr
	, AVL_Node_t *critical_node
	, AVL_Node_t *critical_node_parent) {
	throw exception("not implemented yet! Work to do here!!!");
}


bool AVL_insert(AVL_Node_t *&root_ptr
	, Atom_t value
	, AVL_cmp_func_t cmp)
{
	AVL_Node_t *critical_node = nullptr;
	AVL_Node_t *critical_node_parent = nullptr;
	AVL_Node_t *parent_for_new_node = nullptr;

	//look for the future parent of the node to be attached
	//but also detect the critical node and its parent
	//while going down the tree
	if (!AVL_get_the_future_parent_of_node_to_add_and_critical_node_and_its_parent(
		root_ptr
		, value
		, parent_for_new_node
		, critical_node
		, critical_node_parent
		, cmp)) {
		return false;
	}

	AVL_attach_new_node(root_ptr, critical_node, value, parent_for_new_node, cmp);

	AVL_update_balance_factor_down_of_including(critical_node, value, cmp);

	AVL_detect_and_fix_unbalance(root_ptr, critical_node, critical_node_parent);

	return true;
}

/////////////////////////
///Test support functions
/////////////////////////

///Compare two integer atoms
int cmp_int(void const *p1, void const *p2) {
	return *(int*)p1 - *(int*)p2;
}


///Swap the values of two integers (used in shuffling)
void swap(int &a, int &b) {
	int t = a;
	a = b;
	b = t;
}


///Generates a dinamycaly allocated ascending vector with values from 0 to n-1
int *prepare_ascending_vector(int n) {
	int *v = new int[n];
	for (int i = 0; i < n; ++i) {
		v[i] = i;
	}
	return v;
}


///Shuffles the values withing a vector with n components
void shuffle_vector(int *v, int n) {
	//uncomment folowing line to get different random sequence
	//otherwise the same random sequence is generated on each run
	//srand(time(nullptr));
	for (int i = 0; i < n; ++i) {
		swap(v[i], v[rand() % n]);
	}
}


///Helpful in debuging the structure of the AVL tree
/// displays node value, its balance factor in parantheses and followed by L> or R> then the value of the
///  respective child
void print_map(AVL_Node_t const *bst) {
	if (nullptr == bst) {
		return;
	}
	if (nullptr != bst->left) {
		cout << *(int*)bst->data << "(" << bst->bf << ")" << "L>" << *(int*)bst->left->data << ",";
	}
	if (nullptr != bst->right) {
		cout << *(int*)bst->data << "(" << bst->bf << ")" << "R>" << *(int*)bst->right->data << ",";
	}
	print_map(bst->left);
	print_map(bst->right);
}


///Helpful in debugging to track that the order and values are kept
void print_in_ord(AVL_Node_t const *bst) {
	if (nullptr == bst) {
		return;
	}
	print_in_ord(bst->left);
	cout << *(int*)bst->data << " ";
	print_in_ord(bst->right);
}


///AVL insert test function
void test_AVL_insert(int n) {
	AVL_Node_t *avl = nullptr;

	int *v = prepare_ascending_vector(n);
	shuffle_vector(v, n);

	cout << "Inserting..." << endl;
	for (int i = 0; i < n; ++i) {
		//generic trees work with pointers, so the user data needs to be dinamycaly allocated
		int *user_data_ptr = new int;
		//init the user data
		*user_data_ptr = v[i];
		cout << v[i] << endl;
		if (!AVL_insert(avl, user_data_ptr, cmp_int)) {
			throw exception("not added?");
		}

#ifdef DEBUG_MODE
		print_in_ord(avl);
		cout << endl;
		print_map(avl);
		cout << endl;
#endif
	}

#ifdef DEBUG_MODE
	//list the content for checking
	cout << endl << "In Order print" << endl;
	print_in_ord(avl);
	cout << endl;
#endif

	shuffle_vector(v, n);

	for (int i = 0; i < n; ++i) {
		if (AVL_insert(avl, &v[i], cmp_int)) {
			cout << v[i] << " ";
			throw exception("double add?");
		}
	}

	delete[] v;
	cout << endl;
	cout << "All insert tests seem to have passed!" << endl;
	//still needs to free memory used by the tree!!!
}


///Runs the tests and displays eventual execptions
void main() {
	try {
		test_AVL_insert(40);
	}
	catch (exception e) {
		cout << e.what();
	}
}