//�2017 TP

//*** includes...

//includes C++ specific header with integer type definitions ***
// like int8_t, uint8_t, int16_t, uint16_t, int32_t, ... definitions
//  that make the binary representation clear compared to types like int, short, long
#include <cstdint>

//includes C++ specific header that also contains NULL definition
#include <cstddef>

//for console input output
#include <iostream>

//for easy access to cin, cout objects and others, without fully qualified name like std::cin
using namespace std;

//***type definitions to be used in the current lab***

//the type of the elements handled in vectors
typedef int32_t ELEMENT_t;

//the type for the indices within the lists (limits the maximum number of nodes)
typedef uint32_t ELEMENT_COUNT_t;
#define LIST_SIMPLE_ELEMENT_NOT_FOUND UINT32_MAX

//the type for the linked list node (called NODE to differentiate from the actual element of the list)
typedef struct _NODE_SIMPLE_t {
	ELEMENT_t data;
	struct _NODE_SIMPLE_t *next;
} NODE_SIMPLE_t;

//***functions associated with the current data structure***

//returns true if list is empty (head is NULL)
bool LIST_SIMPLE_is_empty(NODE_SIMPLE_t const * head) {
	//TODO: write your own implementation
	return false;
}

//insert the element data to the front of the list specified by head
// return true if insertion is successful, false if not
bool LIST_SIMPLE_insert_front(NODE_SIMPLE_t *&head, ELEMENT_t data) {
	//TODO: write proper implementation for insert!
	return false;
}

//get the number of elements in the list
ELEMENT_COUNT_t LIST_SIMPLE_count(NODE_SIMPLE_t const *head) {
	return 0;
}

//print all elements of the list (without changing it!)
// that's why head is constrainted to a pointer to a constant
void LIST_SIMPLE_print(NODE_SIMPLE_t const * head) {
	//TODO: write your own print implementation
}

//try to get the value of the node identified by the index parameter
// the value is transfered to the data parameter
//  returns true if the specified index exists in the lists, false otherwise
bool LIST_SIMPLE_get(NODE_SIMPLE_t const *head, ELEMENT_COUNT_t index, ELEMENT_t &data) {
	//TODO: write your own implementation
	return false;
}

//insert the element data to the specified index in the list
// returns true if the element could be inserted at the specified index
bool LIST_SIMPLE_insert(NODE_SIMPLE_t *&head, ELEMENT_t data, ELEMENT_COUNT_t index = 0) {
	//TODO: write proper implementation for insert!
	return false;
}

//removes the front element and deallocates its node
bool LIST_SIMPLE_remove_front(NODE_SIMPLE_t *&head) {
	//TODO: write your own print implementation
	return false;
}

//delete all elements of the list (and dealloc all the used memory!)
void LIST_SIMPLE_clear(NODE_SIMPLE_t *&head) {
	//TODO: write your own print implementation (maybe use remove_front and is_empty?)
}

//gets the index of the first node that contains a value equal to the data parameter
// parameter start_at_index is used to skip all nodes before the given index
//  returns the index of the node containing the searched data or LIST_SIMPLE_ELEMENT_NOT_FOUND otherwise
ELEMENT_COUNT_t LIST_SIMPLE_index_of(NODE_SIMPLE_t const * head, ELEMENT_t data, ELEMENT_COUNT_t start_at_index = 0) {
	//TODO: write your own implementation
	return LIST_SIMPLE_ELEMENT_NOT_FOUND;
}

//check if a node contains the same value as the data parameter
// returns true if the element exists in the list
bool LIST_SIMPLE_contains(NODE_SIMPLE_t const * head, ELEMENT_t data) {
	//TODO: write your implementation (maybe use index_of function to solve this?)
	return false;
}

//removes the node with the given index
// returns true if the index existed
bool LIST_SIMPLE_remove_at(NODE_SIMPLE_t *&head, ELEMENT_COUNT_t index) {
	//TODO: write your own print implementation
	return false;
}

//removes the first encountered node containing the selected data
// but skips all nodes with indices lower than start_at_index parameter
//  returns the index of the node removed or LIST_SIMPLE_ELEMENT_NOT_FOUND otherwise
ELEMENT_COUNT_t LIST_SIMPLE_remove(NODE_SIMPLE_t *&head, ELEMENT_t data, ELEMENT_COUNT_t start_at_index = 0) {
	//TODO: write your own implementation!
	return LIST_SIMPLE_ELEMENT_NOT_FOUND;
}

//first task tests
void P1() {
	NODE_SIMPLE_t *list = NULL;
	ELEMENT_t x;
	//read first element
	cin >> x;
	while (x) {
		//try to add it to the list
		if (!LIST_SIMPLE_insert_front(list, x)) {
			//if not added...
			cout << "Not enough memory or other problem encountered? Quiting..." << endl;
			cin.get();
			exit(EXIT_FAILURE);
		}
		//read next element
		cin >> x;
	}
	LIST_SIMPLE_print(list);
	//TODO: solve the other tasks for this problem

	//making sure no memory leak occur...
	LIST_SIMPLE_clear(list);
}

//second task tests
void P2() {

}

int main() {
	P1();
	P2();
	//continue with other tasks...

	return 0;
}
