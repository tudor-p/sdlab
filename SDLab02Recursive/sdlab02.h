#pragma once
#include <cstdint>
#include <cstddef>

//prints the first n natural numbers without using for, while or goto
//
// @param n - the maximum number to be printed
// @return - nothing
void SDLAB02_print_first_n_natural_numbers_recursively(uint32_t n);

//calculates the factorial function recursively
//
// @param n - the argument for the factorial function
// @return the value for n!
uint64_t SDLAB02_factorial_recursive(uint16_t n);

//calculates the factorial function non-recursively (for comparison purposes with the recursive version)
//
// @param n - the argument for the factorial function
// @return the value for n!
uint64_t SDLAB02_factorial(uint16_t n);

//calculates the greates common divisor for a and b recursively
//
// @param a - first number
// @param b - second number
// @return the greatest common divisor
uint64_t SDLAB02_gcd_recursive(uint64_t a, uint64_t b);

//calculated the value of "x" raised to the positive integer power "power"
//
// @param x - the number to be raised to the power
// @param power - the number of times x should be multiplied with itself
// @returns the value x^power
float SDLAB02_power_float_recursive(float x, uint32_t power);

//calculates the aproximated value of e^x (without recursive code)
//
// @param x - real number representing the power at which e constant should be raised
// @param n_aprox_terms - nu number of terms to be calculated for the series
// @returns aproximated value of e^x
float SDLAB02_exponential_float_aprox(float x, uint32_t n_aprox_terms);

//calculated the aproximated value of e^x (recursive code)
//
// @param x - real number representing the power at which e constant should be raised
// @param n_aprox_terms - nu number of terms to be calculated for the series
// @returns aproximated value of e^x
float SDLAB02_exponential_float_aprox_recursive(float x, uint32_t n_aprox_terms);

//binary searches a value within a sorted vector with a known number of values
//
// @param vector - pointer to the first element of the vector
// @param left - the start index in the vector (inclusive) for the sequence to be searched
// @param right - the end index in the vector (exclusive) for the sequence to be searched
// @param elem - the value to be searched within the vector
// @returns the index of elem when found or UINT32_MAX
//     when the element is not found
uint32_t SDLAB02_binary_search_recursive(uint32_t *vector
	, uint32_t left, uint32_t right
	, uint32_t elem);

//calculates the term numer term_n from the fibonacci sequence
//
// @param term_n - the number of the term to be calculated
// @return the value of the n-th term in the fibonnaci sequence
uint64_t SDLAB02_fibonacci_term(uint16_t term_n);

//checks if the first integer parameter is prime
//
// @param x_to_check - number to be checked for prime
// @param i - current divisor to be tested (default value two, no test, just recursive call)
// @return true if x_to_check is prime
//hint: try to optymize the stop condition!
bool SDLAB02_is_prime(uint64_t x_to_check, uint64_t i = 2);

//prints all the permutation of the first n natural non zero numbers
// @param n - the number of terms in permutation
// @param depth - current depth of permutation (default value 0, helps initializing the state vectors)
//   when depth is zero the space needed for storing the current permutation state is stored
// @return the number of generated permutations
uint32_t SDLAB02_permutations(uint8_t n, uint8_t depth = 0);

//prints the movements used for solving the hanoi towers problem
//
// @param n - the number of disks to be moved
// @param ti - index of the initial place (ti!=tf, ti!=tm)
// @param tf - index of the final place (tf!=tm)
// @param tm - index of the auxiliary place
// @return the total number of movements used to solve current configuration
uint32_t SDLAB02_hanoi(uint16_t n, uint16_t ti, uint16_t tf, uint16_t tm);

//searches recursively (depth first) the bordered (with wall symbols '#') labirinth matrix 
// starting the positiong (i,j) and
// looking for the destination position coded with 'F' characeter 
// @param labirinth - pointer to the arrray of pointers that point to the arrray of
//                     values on each labirinth row
// @param i, j - coordinates of the current position to start searching from
// @param step - the current step number - default 0 on the initial call
// @return true if a path to destination was found
bool SDLAB02_labirint_search(uint8_t **labirinth, uint8_t i, uint8_t j, uint32_t step = 0);

//calculates the maximum possible distance in km traveled by a jeep that consumes c litres
// of gas per 100km, given that it starts with an empty tank of r litres
// and n canisters of r litres each. the tank can only be filled when empty and the
// jeep can transport only one canister at once.
//
// @param r - size in litres of the gas tank and of each gas canister
// @param c - consumption of c litres/100km
// @param n - available number of gas canisters at current location
// @return the total distance that can be traveled away from current location, given
//             the previously mentioned restrictions
double SDLAB02_jeep_trip(double r, double c, uint16_t n);

//prints the elements of the input square matrix with odd dimention in spiral
// starting with the top left corner towards the top right and so on
//
// @param matrix - pointer to the array of pointers to the row arrays of the matrix
// @param dimmension_odd - the dimmension of the input square matrix (must be odd!)
// @param i - row number of the starting postion of the spiral (default top line)
// @param j - column number of the starting postion of the spiral (default left column)
void SDLAB02_matrix_spiral_print(uint16_t **matrix, uint8_t dimmension_odd, uint8_t i = 0, uint8_t j = 0);

//rotates the elements of the input square matrix with odd dimmension, in place,
// arround the matrix center, 90 degrees in trigonometric direction
//
// @param matrix - pointer to the array of pointers to the row arrays of the matrix
// @param dimmension_odd - the dimmension of the input square matrix (must be odd!)
// @param i - row number of the starting postion of the spiral (default top line)
// @param j - column number of the starting postion of the spiral (default left column)
void SDLAB02_matrix_rotate_in_place(uint16_t **matrix, uint8_t dimmension_odd, uint8_t i = 0, uint8_t j = 0);

//determines the maximum sum of a continuous subsequesnce of the input vector
//
// @param vector - vector of integers
// @param length - the length of the input vector
// @param section_start - pointer to a value used for returning the index of the start of the found sequence
// @param section_length - pointer to a value used for returning the length of the found sequence 
// @return the sum of the identified sequence
int32_t SDLAB02_section_with_max_sum(int16_t *vector, uint16_t length, uint16_t *section_start = NULL, uint16_t *section_end = NULL);

//searches a indices of a specific value 
// in a matrix with the values sorted ascending on each of the rows and columns
//
// @param matrix - pointer to the array of pointers to the row arrays of the matrix
// @param im - the index of the last row that should be searched in the matrix (including)
// @param jm - the index of the last column that should be searched in the matrix (including)
// @param x - the value to be searched in the matrix
// @param i - pointer to the value used to return the index of the row containing the value x
// @param j - pointer to the value used to return the index of the column containing the value x
// @param i0 - the index of the first row in the matrix that should be searched (including)
// @param j0 - the index of the first column in the matrix that shou be searched (including)
// @return true if the value was find and (i,j) is valid, false otherwise
bool SDLAB02_optimum_search_in_sorted_matrix(uint16_t **matrix, uint8_t im, uint8_t jm, uint8_t x, uint8_t *i, uint8_t *j, uint8_t i0 = 0, uint8_t j0 = 0);

