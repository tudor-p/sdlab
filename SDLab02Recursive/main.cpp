#include <iostream>
#include <fstream>
#include <cstring>

#include "sdlab02.h"

using namespace std;

void print_first_n_numbers_test() {
	SDLAB02_print_first_n_natural_numbers_recursively(5);
	//TODO: add other tests
}

void factorial_test() {
	//TODO: write some simple code to test that your factorial implementations work!
}

void gcd_test() {

}

void exponential_test() {
	
}

void fibonnaci_test() {

}

void prime_test() {

}

void binary_search_test() {
	
}

//test perm problem for order n
void perm_test(uint8_t n) {
	cout << "P(" << (uint16_t)n << ")=" << SDLAB02_permutations(n) << endl;
}

//test hanoi problem for s disks
//
// @param d - number of disks to start with
void hanoi_test(uint16_t d) {
	cout << "Discuri: " << d << " Mutari: " << SDLAB02_hanoi(d, 0, 1, 2) << endl;;
}

//reads the labirinth structure from file 
// and returns a pointer to an array of arrays representing the matrix
//
// @param path - file system path to file (properly escaped if required)
// @param im, jm - pointers to the values used to return the dimmensions of the matrix
// @param is, js - pointers to the values used to return the coordinates of the start position
//                     the start position is being replaced with ' ' during parsing
// @return the pointer to the array of pointers that point to the array of values of each
//                            row of the matrix
uint8_t **read_labirinth(char* path, uint8_t *im, uint8_t *jm, uint8_t *is, uint8_t *js) {
	ifstream fin(path);
	if (!fin.is_open()) {
		cout << "Could not open file: " << path << endl;
		exit(EXIT_FAILURE);
	}
	uint16_t x, y;
	fin >> x >> y;
	*im = x;
	*jm = y;
	uint8_t **m = NULL;
	try { m = new uint8_t*[x + 1]; }
	catch (exception e) { m = NULL; }
	if (NULL == m) {
		cout << "Could not allocate space for matrix lines" << endl;
		exit(EXIT_FAILURE);
	}
	m[x] = NULL; //leave last line unallocated to mark end of matrix
	fin.get();
	for (uint8_t i = 0;i < x;++i) {
		int char_count_to_read = y + 1; //+1 to keep the \x0 termination char
		try { m[i] = new uint8_t[char_count_to_read]; }
		catch (exception e) { m[i] = NULL; }
		if (NULL == m[i]) {
			cout << "Could not allocate space for matrix columns" << endl;
			exit(EXIT_FAILURE);
		}
		fin.getline((char*)(m[i]), char_count_to_read);
		if ((fin.gcount() != char_count_to_read) || (fin.fail())) {
			cout << "Incomplete input line" << endl;
			exit(EXIT_FAILURE);
		}
		for (uint8_t j = 0;j < y;++j) {
			if ('I' == m[i][j]) {
				*is = i;
				*js = j;
				m[i][j] = ' ';
			}
		}
	}
	return m;
}
//helper function that frees the memory associated with a labirinth
// @param m - pointer to the array of pointers to the array of values of the labirinth rows
// @param im - number of rows in the array (needed for freeing each allocated row)
void free_labirint(uint8_t **m, uint8_t im) {
	for (uint8_t i = 0;i < im;++i) {
		void *p = m[i];
		delete p;
		m[i] = NULL;
	}
	delete[] m;
	m = NULL;
}

//test funtion for labirinth problem
// - reads labirinth matrix from given file
// - invokes the search for solution
// - displays the solution vector
//
// @param path - pointer to a string containing the path to the input file describing
//                  the map of the labirinth with '#', ' ', 'I', 'F' symbols
void labirint_test(char *path) {
	cout << "Labirinth test" << endl;
	uint8_t im, jm, is, js;
	uint8_t **m = read_labirinth(path, &im, &jm, &is, &js);
	cout << "rows: " << (uint16_t)im << " cols: " << (uint16_t)jm << " start(" << (uint16_t)is << "," << (uint16_t)js << ")" << endl;
	if (SDLAB02_labirint_search(m, is, js)) {
		//afiseaza solutia
	}
	free_labirint(m, im);
}

void jeep_test() {

}

//helper function for preparing a square matrix of dimmension s
// useful for the matrix problems
//
// @param s - dimmension of the matrix to be filled with numbers from 0 to s*s-1
// @return pointer to the array of pointers that point to the arrays of values of the rows
uint16_t **prep_matrix(uint8_t s) {
	uint16_t **m = new uint16_t*[s];
	for (uint8_t i = 0;i < s;++i) {
		m[i] = new uint16_t[s];
		for (uint8_t j = 0; j < s;++j) {
			m[i][j] = i * s + j;
		}
	}
	return m;
}
//helper function for displaying the square matrix
//
// @param m - the pointer to the array of pointers that point to the arrays of values of the rows
// @param s - the dimmension of the square matrix
void print_matrix(uint16_t **m, uint8_t s) {
	for (uint8_t i = 0;i < s;++i) {
		for (uint8_t j = 0; j < s;++j) {
			cout << m[i][j] << " ";
		}
		cout << endl;
	}
}

//helper function for deallocating the memory of the square matrix
//
// @param m - the pointer to the array of pointers that point to the arrays of values of the rows
// @param s - the dimmension of the square matrix
void clean_matrix(uint16_t **m, uint8_t s) {
	for (uint8_t i = 0;i < s;++i) {
		delete[] m[i];
	}
	delete[] m;
}

//test the spiral print function
//
// @param s - the dimmesion of the matrix
void spiral_test(uint8_t s) {
	if (0 == (s & 0x1))
	{
		cout << "Dimmension must be odd!" << endl;
		exit(EXIT_FAILURE);
	}
	cout << "Matrix spiral print test" << endl;
	uint16_t **m = prep_matrix(s);
	print_matrix(m, s);
	SDLAB02_matrix_spiral_print(m, s);
	clean_matrix(m, s);
}

//test the matrix rotate function
//
// @param s - the dimmesion of the matrix
void rotate_test(uint8_t s) {
	if (0 == (s & 0x1))
	{
		cout << "Dimmension must be odd!" << endl;
		exit(EXIT_FAILURE);
	}
	cout << "Matrix rotation test" << endl;
	uint16_t **m = prep_matrix(s);
	print_matrix(m, s);
	cout << endl;
	SDLAB02_matrix_rotate_in_place(m, s);
	print_matrix(m, s);
	clean_matrix(m, s);
}

//test the max sum subsequence problem
void max_sum_test() {
	const uint16_t s = 10;
	cout << "Max sum subsequence test" << endl;
	int16_t v3[s] = { 1, 2, 3, -4, 5, 6, 7, 8, 9, 10 };
	cout << SDLAB02_section_with_max_sum(v3, s) << endl;
	int16_t v0[s] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	cout << SDLAB02_section_with_max_sum(v0, s) << endl;
	int16_t v[s] = { -1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	cout << SDLAB02_section_with_max_sum(v, s) << endl;
	int16_t v1[s] = { 1, -2, 3, 4, 5, 6, 7, 8, 9, 10 };
	cout << SDLAB02_section_with_max_sum(v1, s) << endl;
	int16_t v2[s] = { 1, 2, -3, 4, 5, 6, 7, 8, 9, 10 };
	cout << SDLAB02_section_with_max_sum(v2, s) << endl;

}

void matrix_search_test() {

}

void main() {
	print_first_n_numbers_test();
	
	factorial_test();
	
	//gcd_test();
	
	//exponential_test();
	
	//fibonnaci_test();
	
	//prime_test();

	//binary_search_test();

	//fibonnaci_test();

	//prime_test();

	//labirint_test("labirinth.txt");
	//perm_test(10);
	//hanoi_test(10);
	//max_sum_test();
	//rotate_test(7);
	//spiral_test(7);	
}
