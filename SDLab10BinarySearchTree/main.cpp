#include <iostream>
#include <ctime>
using namespace std;

#define DEBUG_MODE

////////////////////////////////////
///Generic BST Types and Functions
////////////////////////////////////
struct BST_Node_t {
	///pointer to user data - constant pointer to constant data
	///constatnt pointer - because once a data is associated with a node should not be changed
	/// to point to another data (which might corupt the tree)
	///pointer to constant - the data pointed by the tree node will not be changed by the 
	/// tree operations and should not be changed externally either
	void const * const data_ptr;
	///pointers to the left and right descendents
	BST_Node_t *left, *right;
	///Structure constructor for initializing the constant fields (works in C++ only)
	BST_Node_t(void const * _data_ptr) : data_ptr(_data_ptr) {};
};

///Type for comparison function used to establish the order relationship within the tree structure
/// @param data_ptr1 - generic pointer to the first value of the comparison
/// @param data_ptr2 - generic pointer to the second value of the comparison
/// @returns an integer value which is zero if the values refered by the pointers are equal
///             or a negative value if the first value is smaller than the second value
///             or a positive vaule if the first value is higher than the second value
typedef int(*BST_cmp_func_t)(void const * data_ptr1, void const * data_ptr2);

///Inserts in the BST a new node with a copy of the recieved data_ptr
/// @param bst - pointer to current root of the binary search tree/subtree
/// @param data_ptr - pointer to the value to be added to the bintary tree
/// @param compare - pointer to function that knows how to compare the values referred by the generic pointers
/// @returns true if the insertion succeeded 
///           or false if the value already exists in the tree
bool BST_insert(BST_Node_t *&bst, void const * data_ptr, BST_cmp_func_t compare) {
	throw exception("not implemented yet! Work to do here!!!");
}

///Searches in the BST for a node containing a pointer to data that is equivalent with the data specified by the data_ptr
/// from the point of view of the compare function
/// @param bst - pointer to the root of the binary search tree/subtree
/// @param data_ptr - pointer to the data that is to be searched for an equivalent instance in the tree
/// @param compare - pointer to function that establishes equivalence between two data or the their order
/// @returns a pointer to the data from the tree (preferably to the data and not to the node)
///               or null if the data was not found
void const *BST_search(BST_Node_t const * bst
	, void const * data_ptr, BST_cmp_func_t compare) {
	throw exception("not implemented yet! Work to do here!!!");
}

///Searches for the gratest node starting from the root bst and disconnects that node then returns it
/// @param bst - the root of the binary search subtree in which the maximum is to be searched
/// @returns pointer to the node that just has been disconnected from the tree
/// NOTE: no compare function is needed since the maximum is always on the right most of the tree
/// NOTE: it will always return a not null value! otherwise it is not invoked at all
BST_Node_t *BST_remove_and_return_greatest_node(BST_Node_t *&bst) {
	throw exception("not implemented yet! Work to do here!!!");
}

///Tries to remove the first node with pointer to data equivvalent with data_ptr from the point of view of compare function
/// @param bst - pointer to the root of the tree
/// @param data_ptr - pointer to the data to be searched and removed
/// @param compare - compare function used to establish equivalence and order in the tree
/// @returns - pointer to the data that has been disconnected from the tree
///              or nullptr if the data was not found
void const *BST_remove(BST_Node_t *&bst, void const * data_ptr, BST_cmp_func_t compare) {
	throw exception("not implemented yet! Work to do here!!!");
}


////////////////////////////////////
///Test Support Functions
////////////////////////////////////

/// example of compare function for integers
int cmp_int(void const *p1, void const *p2) {
	return *(int*)p1 - *(int*)p2;
}

/// reverses a and b values
void swap(int &a, int &b) {
	int t = a;
	a = b;
	b = t;
}


int *prepare_ascending_vector(int n) {
	int *v = new int[n];
	for (int i = 0;i < n;++i) {
		v[i] = i;
	}
	return v;
}


void shuffle_vector(int *v, int n) {
	srand(time(nullptr));
	for (int i = 0;i < n;++i) {
		swap(v[i], v[rand() % n]);
	}
}


void print_in_ord(BST_Node_t const *bst) {
	if (nullptr == bst) {
		return;
	}
	print_in_ord(bst->left);
	cout << *(int*)bst->data_ptr << " ";
	print_in_ord(bst->right);
}


///BST Test function for generic implementation
///Inserts n nodes
///then checks that double insertions fail
///then removes all nodes in random order
/// tries to double remove each node and checks that it fails
///checks that the tree is empty
void test_BST(int n) {
	BST_Node_t *bst = nullptr;

	int *v = prepare_ascending_vector(n);
	shuffle_vector(v, n);

	cout << "Inserting..." << endl;
	for (int i = 0; i < n;++i) {
		//generic trees work with pointers, so the user data needs to be dinamycaly allocated
		int *user_data_ptr = new int;
		//init the user data
		*user_data_ptr = v[i];
#ifdef DEBUG_MODE
		cout << v[i] << " ";
#endif
		if (!BST_insert(bst, user_data_ptr, cmp_int)) {
			throw exception("not added?");
		}
	}
	//list the content for checking

#ifdef DEBUG_MODE
	cout << endl << "In Order print" << endl;
	print_in_ord(bst);
	cout << endl;
#endif

	shuffle_vector(v, n);

	for (int i = 0; i < n;++i) {
		if (BST_insert(bst, &v[i], cmp_int)) {
#ifdef DEBUG_MODE
			cout << v[i] << " ";
#endif
			throw exception("double add?");
		}
	}

	shuffle_vector(v, n);

	cout << "Searching..." << endl;
	for (int i = 0; i < n; ++i) {
#ifdef DEBUG_MODE
		cout << v[i] << " ";
#endif
		int *data_ptr = (int*)BST_search(bst, &v[i], cmp_int);
		if (nullptr == data_ptr || (*data_ptr) != v[i]) {
			throw exception("something lost?");
		}
	}

	shuffle_vector(v, n);

	cout << endl << "Removing..." << endl;
	for (int i = 0; i < n;++i) {
#ifdef DEBUG_MODE
		cout << v[i] << " ";
#endif
		//result is a pointer to the data provided by the user
		void const *result = BST_remove(bst, &v[i], cmp_int);
		if (!result) {
			cout << i << " " << v[i];
			throw exception("not found!");
		}

		delete result; //it is the user's responsability to clear the memory he allocated
		//try to remove again (actually checks if the first remove worked correctly)
		result = BST_remove(bst, &v[i], cmp_int);
		if (result) {
			throw exception("removed, but found?");
		}
	}
	if (nullptr != bst) {
		throw exception("not empty?");
	}
	delete[] v;
	cout << endl;
	cout << "All tests seem to have passed!" << endl;

}


///Runs the tests and displays eventual execptions
void main() {
	try {
		test_BST(40);
	}
	catch (exception e) {
		cout << e.what();
	}
}
