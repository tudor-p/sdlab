//#define TIXML_USE_STL //uncomment if support for Standard Template Library is wanted

#include <iostream>

#include "tinyxml.h"
#include "tinystr.h"

using namespace std;

int main()
{
	TiXmlDocument doc("SampleData.xml");
	doc.LoadFile();
	doc.Print();

	TiXmlNode* node = 0;
	TiXmlElement* studentsListElement = 0;
	TiXmlElement* studentElement = 0;

	//un nod este clasa de baza pentru clasele ce descriu componentele ce se pot gasi 
	//in cadrul unui document xml in afara atributelor (comentarii, elemente, declaratii, etc)

	// un nod poate avea frati, parinti si copii

	//cautam primul nod al documentului
	//in cazul considerat, nodul <Studenti> (incapsuleaza o lista de noduri Student)
	node = doc.FirstChild("Studenti");
	//daca acest nod nu exista, se iese din program
	assert(node);
	//incercam transformarea acestui nod intr-un element
	studentsListElement = node->ToElement();
	//daca transformarea esueaza, iesim din program
	assert(studentsListElement);

	//cautam in continuare primul copil al nodului <Studenti>
	node = studentsListElement->FirstChildElement();

	//cat timp avem noduri de parcurs in interiorul nodului Studenti
	while (node)
	{
		//incercam transformarea nodului curent intr-un element
		studentElement = node->ToElement();
		//daca nu se reuseste transformarea sa intr-un element din cauza unor erori se iese din program
		assert(studentElement);

		//cautam primul atribut al elementului <Student>
		TiXmlAttribute* studentAttribute = studentElement->FirstAttribute();


		//cat timp atributul urmator exista
		while (studentAttribute)
		{
			//afisam datele referitoare la acest atribut
			cout << "Nume atribut: " << studentAttribute->Name();
			cout << " Valoare atribut: " << studentAttribute->Value() << endl;

			//trecem la atributul urmator
			studentAttribute = studentAttribute->Next();
		}

		TiXmlNode* childNodeForStudent = 0;
		childNodeForStudent = studentElement->FirstChildElement();

		while (childNodeForStudent)
		{
			//afisam datele referitoare la aceste noduri copii
			TiXmlElement* elementOfStudent = childNodeForStudent->ToElement();
			cout << "Nume element: " << childNodeForStudent->Value();
			cout << " Text element: " << elementOfStudent->GetText() << endl;

			//trecem la urmatorul nod copil
			childNodeForStudent = elementOfStudent->NextSibling();
		}

		//trecem la urmatorul nod din cadrul elementului <Studenti>
		node = node->NextSibling();
	}


	TiXmlElement *m = new TiXmlElement("Student");
	doc.SaveFile();
	system("PAUSE");
	return 0;
}