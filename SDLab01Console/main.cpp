//�2016 TP

//includes C++ specific header with integer type definitions 
//like int16_t uint16_t, definitions
//that make the binary representation clear compared to types like int, short, long
#include <cstdint>

//includes C++ specific header that also contains NULL definition
#include <cstddef>

//for console input output
#include <iostream>

//for real time measurement (please note the difference between CPU time and real time)
#include <ctime>

//for easy access to cin and cout objects without fully qualified name like std::cin
using namespace std;

//type definitions to be used in the current lab
// the type of the elements handled in vectors
typedef uint32_t SDLAB01_ELEMENT_t;
// the type for the sizes of the vectors (or indices within)
typedef uint32_t SDLAB01_ELEMENT_COUNT_t;
// the type for the counter used for checking performance
//   (like comparison count, or recursive call count)
typedef uint64_t SDLAB01_PERFORMANCE_COUNTER_t;

// type for pointer to function that is accepted by the function that tests sorting functions
typedef void (*sortFuntion_t)(SDLAB01_ELEMENT_t *vector, SDLAB01_ELEMENT_COUNT_t count
	, SDLAB01_PERFORMANCE_COUNTER_t *compare_count);

//takes the pointer to the vector and the count of elements and sorts the vector inplace
void SDLAB01_buble_sort(SDLAB01_ELEMENT_t *vector, SDLAB01_ELEMENT_COUNT_t count
	, SDLAB01_PERFORMANCE_COUNTER_t *compare_count = NULL) {
	// TODO: improve this buble sort! 
	//       (this is "almost" the most inefficient buble sort algorithm possible)
	//       (use the option presented in the lab document)
	for (SDLAB01_ELEMENT_COUNT_t i = 1; i < count; ++i) {
		for (SDLAB01_ELEMENT_COUNT_t j = 0; j < count - i; ++j) {
			if (vector[j] > vector[j + 1]) {
				SDLAB01_ELEMENT_t t = vector[j];
				vector[j] = vector[j + 1];
				vector[j + 1] = t;
			}
		}
	}
}

//takes the pointer to the vector and the count of elements and sorts the vector inplace
void SDLAB01_selection_sort(SDLAB01_ELEMENT_t *vector, SDLAB01_ELEMENT_COUNT_t count
	, SDLAB01_PERFORMANCE_COUNTER_t *compare_count = NULL) {
	// TODO: implement selection sort
}

//takes the pointer to the vector and the count of elements and sorts the vector inplace
void SDLAB01_insertion_sort(SDLAB01_ELEMENT_t *vector, SDLAB01_ELEMENT_COUNT_t count
	, SDLAB01_PERFORMANCE_COUNTER_t *compare_count = NULL) {
	// TODO: implement insertion sort
}

//takes the pointer to the vector and the count of elements, searches for element elem
//returns the index of elem when found
//   or a value higher or equal to "count" when not found
SDLAB01_ELEMENT_COUNT_t SDLAB01_binary_search(SDLAB01_ELEMENT_t *vector
	, SDLAB01_ELEMENT_COUNT_t count
	, SDLAB01_ELEMENT_t elem
	, SDLAB01_PERFORMANCE_COUNTER_t *compare_count = NULL) {
	return 0; //TODO: replace with the proper return value
}

//tests the given sort function by providing the input vector [test_size, test_size-1, ..., 2, 1]
// and checks that the output vector is [1, 2, ... test_size-1, test_size]
// and displays the measured time needed to run the code
void testSortFunctionWithReversedVector(sortFuntion_t sort_function_to_be_tested, SDLAB01_ELEMENT_COUNT_t test_size) {
	SDLAB01_ELEMENT_t *v = new SDLAB01_ELEMENT_t[test_size];
	for (SDLAB01_ELEMENT_COUNT_t i = 0; i < test_size; ++i) {
		v[i] = test_size - i;
	}
	SDLAB01_PERFORMANCE_COUNTER_t comparations = 0;
	clock_t begin = clock();
	sort_function_to_be_tested(v, test_size, &comparations);
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	for (SDLAB01_ELEMENT_COUNT_t i = 0; i < test_size; ++i) {
		if (v[i] != (i + 1))
		{
			cout << "Test Failed: The " << test_size << " elements were not sorted! v[" << i << "] is " << v[i] << ", but was expected to be " << i + 1 << endl;
			cin.get(); //work arround for getting to see the output error before the console closes
			exit(EXIT_FAILURE);
		}
	}
	cout << "Test Passed: Sorted " << test_size << " elements in " << elapsed_secs << "s" << endl;

	delete[] v;
}

//entry point for the application
int main() {
	//test cases
	cout << "Testing Buble Sort implementation" << endl;
	testSortFunctionWithReversedVector(SDLAB01_buble_sort, 8);
	testSortFunctionWithReversedVector(SDLAB01_buble_sort, 10240);
	testSortFunctionWithReversedVector(SDLAB01_buble_sort, 20480);
	testSortFunctionWithReversedVector(SDLAB01_buble_sort, 40960);
	testSortFunctionWithReversedVector(SDLAB01_buble_sort, 81920);
	
	cout << "Testing Selection Sort implementation" << endl;
	testSortFunctionWithReversedVector(SDLAB01_selection_sort, 8);
	testSortFunctionWithReversedVector(SDLAB01_selection_sort, 10240);
	testSortFunctionWithReversedVector(SDLAB01_selection_sort, 20480);
	testSortFunctionWithReversedVector(SDLAB01_selection_sort, 40960);
	testSortFunctionWithReversedVector(SDLAB01_selection_sort, 81920);
	
	//TODO: add tests for the insertion sort algorithm!

	//TODO: check that your binary sort implementation works as expected!

	return 0;
}

/*
//Alternative main functions for application entry point
void main() {
}

void main(void) {
}

int main() {
	return 0;
}

int main(void) {
	return 0;
}

void main(int argc, char *argv[]) {
}

int main(int argc, char *argv[]) {
	return 0;
}

void main(int argc, char *argv[], other_parameters) {
}

int main(int argc, char *argv[], other_parameters) {
}
*/