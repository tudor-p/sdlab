#include <iostream>
using namespace std;

#include "canonic.h"

#define DIM_EXPR 100


Nod* creareArbore()
{
	char buffer[DIM_EXPR];
	cin >> buffer;
	int length = strlen(buffer);
	Nod* n = parse(buffer, 0, length - 1);
	return n;
}


Nod* CANONIC_create_node(int grad, int data)
{
	Nod *p = (Nod*) new char[sizeof(Nod) - (GRMAX - grad) * sizeof(Nod*)];
	p->grad = 0;
	p->data = data;
	return p;
}

Nod* parse(char const * buffer, int start, int end)
{
	//TODO: use the code in the lab file
	return NULL;
}
int grad(Nod *r)
{
	int gm = 0;
	//TODO: implement
	return gm;
}
void afis(Nod *r)
{
	//TODO: implement
}
double eval(Nod *r)
{
	double rez = 0;
	//TODO: implement
	return rez;
}

void CANONIC_free_nodes_recursive(Nod *&a) {
	//TODO: free recursivele all nodes
}

Nod* CANONIC_clone_leaf_node(Nod *a) {
	Nod *p = CANONIC_create_node(a->grad);
	//TODO: copy the rest of info
	return p;
}

Nod* CANONIC_clone_P_node(Nod *a) {
	Nod *p = CANONIC_create_node(a->grad);
	//TODO: clone all the info
	return p;
}
//Sum to SP nodes in a new one while not changing the original nodes
Nod* CANONIC_convert_to_SP_an_S_node_with_SP_descendent_nodes(Nod *a)
{
	//TODO: implement
	return NULL;
}
//Multiply two product nodes by joing clones of their leaves in one new node while not changing the original nodes
Nod* CANONIC_multiply_P_nodes(Nod *a, Nod *b)
{
	//TODO: implement
	return NULL;
}
//Multiply two SP nodes into a new single SP node while not changing the original nodes
Nod* CANONIC_multiply_SP_nodes(Nod *a, Nod *b)
{
	//TODO: implement
	return NULL;
}

Nod * CANONIC_convert_to_SP_a_leaf_node(Nod * input) {
	//TODO: implement
	return NULL;
}

//Entry point for converting any expression
Nod * CANONIC_convert_to_SP(Nod *input) {
	Nod *sp = NULL;
	if (0 == input->grad) { sp = CANONIC_convert_to_SP_a_leaf_node(input); } //basic leaf case
	else if (1 == input->grad) { sp = CANONIC_convert_to_SP(input); } //basic inner node with just on descendent
	else { //inner nodes with two or more descendents
		Nod *xsp = CANONIC_create_node(input->grad, input->data); //xsp will be in  either ssp or psp form
		for (int i = 0; i < input->grad; ++i) { xsp->vDesc[i] = CANONIC_convert_to_SP(input->vDesc[i]); }
		switch (xsp->data) {
		case '+': //ssp form to sp
			sp = CANONIC_convert_to_SP_an_S_node_with_SP_descendent_nodes(xsp); break;
		case '*': // psp form to sp
			sp = CANONIC_multiply_SP_nodes(xsp->vDesc[0], xsp->vDesc[1]);
			for (int i = 2; i < input->grad; ++i) {
				Nod *tsp = sp;
				sp = CANONIC_multiply_SP_nodes(sp, xsp->vDesc[i]);
				CANONIC_free_nodes_recursive(tsp); //cleaning temporary product structures
			} break;
		default: cout << "Unsupported operation: " << xsp->data << "!" << endl; exit(EXIT_FAILURE); break;
		}
		CANONIC_free_nodes_recursive(xsp); // don't forget to clean temporary structures!
	}
	return sp;
}

int CANONIC_compare_P_nodes(Nod *p1, Nod *p2)
{
	//TODO: implement
	return 0;
}

void CANONIC_sort_factors(Nod *p)
{
	//TODO: implement
}
void CANONIC_sort_SP_terms_and_factors(Nod *r)
{
	//TODO: implement
}
int CANONIC_test_equivalence(Nod *p1, Nod *p2)
{
	//TODO: implement
	return 1;
}

int main()
{
	char c;
	do
	{
		cout << "Introduceti expresia 1: ";
		Nod *a = creareArbore();
		cout << "Grad: " << grad(a) << "\n";
		cout << "Operanzi:";
		afis(a);
		cout << "\nEvaluare: ";
		cout << eval(a) << "\n";
		cout << "Introduceti expresia 2: ";
		Nod *b = creareArbore();
		cout << "Grad: " << grad(b) << "\n";
		cout << "Operanzi:";//
		afis(b);
		cout << "\nEvaluare: ";
		cout << eval(b);

		if (CANONIC_test_equivalence(a, b))
		{
			cout << "\nExpresiiele sunt echivalente!\n";
		}
		else
		{
			cout << "\nExpresiiele nu sunt echivalente!\n";
		}
		//atribuiam de aiurea adrese... eliberam ramuri din a si b...
		//si voiam sa le mai eliberez odata :D
		//stupid me...
		CANONIC_free_nodes_recursive(a);
		CANONIC_free_nodes_recursive(b);

		cout << "\nPress r to retry\n";
		cin >> c;
	} while (c == 'r');
	return 0;
}
