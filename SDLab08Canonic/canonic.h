#pragma once
#define GRMAX 50

struct Nod {
	int data;
	int grad;
	Nod *vDesc[GRMAX];
};

//Allocates and initializes a new node
Nod* CANONIC_create_node(int grad, int data = 0);
//Parse a string buffer containing only digits, +, *, ( and )
// into a generic tree in which the digits are leaves and +, * and no brackets
// @param buffer - the input buffer to be parsed
// @param start - start index in buffer 
// @param end - end index in buffer
// @return pointer to the root node of the tree
Nod* parse(char const *buffer, int start, int end);
//Used to deallocate all the nodes in the tree, recursively, depth first
// @param a - reference to the pointer to the root of the expression tree
void CANONIC_free_nodes_recursive(Nod *&a);
//Clones a simple leaf node (creates an identical node)
Nod* CANONIC_clone_leaf_node(Nod *a);
//Clones a product node (which includes cloning all its leaves)
Nod* CANONIC_clone_P_node(Nod *a);

//Create an SP tree equivalent of a leaf node (creates new S and P nodes and clones the leaf node)
Nod* CANONIC_convert_to_SP_a_leaf_node(Nod *a);
//Create an SP tree equivalent to an S node with SP nodes, while not changing the original nodes (clones all necesary data)
Nod* CANONIC_convert_to_SP_an_S_node_with_SP_descendent_nodes(Nod *a);
//Multiply two product nodes (having just leaf nodes) by joing clones of their leaves in one new node while not changing the original nodes
Nod* CANONIC_multiply_P_nodes(Nod *a, Nod *b);
//Multiply two SP trees into a new single SP node while not changing the original nodes
Nod* CANONIC_multiply_SP_nodes(Nod *a, Nod *b);
//Create an SP tree mathematicaly equivalent to the generic input tree
Nod * CANONIC_convert_to_SP(Nod *input);
//Check if two P tree are identical, returns zero if identical
int CANONIC_compare_P_nodes(Nod *p1, Nod *p2);
//Sort the factors in a P tree
void CANONIC_sort_factors(Nod *p);
//Sort an SP term
void CANONIC_sort_SP_terms_and_factors(Nod *r);
//Tests the trees of two expressions for equivalence
int CANONIC_test_equivalence(Nod *p1, Nod *p2);